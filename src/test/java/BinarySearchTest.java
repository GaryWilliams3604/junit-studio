import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTest {

    @Test
    public void goodBinarySearchTest() {
        int [] nums = {1,3,5,7,9,11,13};
        assertEquals(3, BinarySearch.binarySearch(nums, 7));
    }

    @Test
    public void badBinarySearchTest() {
        int [] nums = {1,3,5,7,9,11,13};
        assertNotEquals(5, BinarySearch.binarySearch(nums, 9));
    }

    @Test
    public void numNotFoundTest() {
        boolean notFound = false;
        int [] nums = {1,3,5,7,9,11,13};
        if(BinarySearch.binarySearch(nums, 10) == -1) {
            notFound = true;
        }
        assertTrue("Number not found", notFound);
    }
}
