import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BalancedBracketsTest {

    //TODO: add tests here
    @Test
    public void goodBracketTest() {
        assertTrue(BalancedBrackets.hasBalancedBrackets("Th[is is go]od"));
    }

    @Test
    public void badBracketTest() {
        assertFalse(BalancedBrackets.hasBalancedBrackets("Th]is is b[ad"));
    }

    @Test
    public void firstCloseBracketTest() {
        assertFalse(BalancedBrackets.hasBalancedBrackets("]Bad Tes[t"));
    }

    @Test
    public void lastOpenBracketTest() {
        assertFalse(BalancedBrackets.hasBalancedBrackets("Bad Test["));
    }

    @Test
    public void openOpenCloseTest() {
        assertFalse(BalancedBrackets.hasBalancedBrackets("[This [is bad]"));
    }

    @Test
    public void closeCloseOpenTest() {
        assertFalse(BalancedBrackets.hasBalancedBrackets("T]his is ]also b]ad"));
    }

    @Test
    public void doubleGoodNest() {
        assertTrue(BalancedBrackets.hasBalancedBrackets("[Thi[s is] a go]od test"));
    }
}
