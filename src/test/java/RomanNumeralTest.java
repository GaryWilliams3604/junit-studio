import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class RomanNumeralTest {

    @Test
    public void testDecToRomanPass() {
        Integer decNum = 9;
        assertEquals("Roman", "IX", RomanNumeral.fromInt(decNum.intValue()));
    }

    @Test
    public void testDecToRomanFail() {
        Integer decNum = 9;
        assertNotEquals("VIIII", RomanNumeral.fromInt(decNum.intValue()));
    }

    @Test
    public void testDecToRomanPass2() {
        Integer decNum = 999;
        assertEquals("Roman", "CMXCIX", RomanNumeral.fromInt(decNum.intValue()));
    }

    @Test
    public void testDecToRomanFail2() {
        Integer decNum = 999;
        assertNotEquals("CMLXXXXIX", RomanNumeral.fromInt(decNum.intValue()));
    }
}
